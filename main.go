package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"time"

	"github.com/SlyMarbo/rss"
	"github.com/ashwanthkumar/slack-go-webhook"
)

type Config struct {
	FogBugz struct {
		FeedUrl string `json:"feedUrl"`
	}
	Slack struct {
		WebhookUrl string `json:"webhookUrl"`
		Recipient  string `json:"recipient"`
		Username   string `json:"username"`
		IntroText  string `json:"introText"`
		Color      string `json:"color"`
	}
}

func (c *Config) Read(filePath string) error {
	data, err := ioutil.ReadFile(filePath)
	if err != nil {
		return err
	}

	err = json.Unmarshal(data, c)
	if err != nil {
		return err
	}

	return nil
}

func fetchFeed(url string) (*rss.Feed, error) {
	feed, err := rss.Fetch(url)
	if err != nil {
		return nil, err
	}
	return feed, nil
}

func buildPayload(config *Config, items []*rss.Item) *slack.Payload {

	attachments := []slack.Attachment{}

	for _, i := range items {
		fallback := fmt.Sprintf("%s (%s)", i.Title, i.Link)
		text := fmt.Sprintf("<%s|%s>", i.Link, i.Title)
		color := &config.Slack.Color

		attachment := slack.Attachment{
			Fallback: &fallback,
			Text:     &text,
			Color:    color,
		}

		attachment.AddField(slack.Field{
			Title: "Category",
			Value: i.Category,
			Short: true,
		})

		attachment.AddField(slack.Field{
			Title: "Opened",
			Value: i.Date.Format(time.RFC822),
			Short: true,
		})

		attachments = append(attachments, attachment)
	}

	return &slack.Payload{
		Text:        config.Slack.IntroText,
		Username:    config.Slack.Username,
		Channel:     config.Slack.Recipient,
		Attachments: attachments,
	}
}

func slackIt(config *Config, payload *slack.Payload) []error {
	err := slack.Send(config.Slack.WebhookUrl, "", *payload)
	if len(err) > 0 {
		return err
	}
	return nil
}

func main() {
	config := &Config{}
	config.Read("./config.json")

	feed, err := fetchFeed(config.FogBugz.FeedUrl)
	if err != nil {
		log.Fatalf("Error fetching feed: %s", err)
	}

	payload := buildPayload(config, feed.Items)

	errs := slackIt(config, payload)
	if len(errs) > 0 {
		log.Fatalf("Error(s) sending to Slack: %s", errs)
	}
}
