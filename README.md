# Casey Jones (Go)

Small tool that posts cases from a FogBugz filter in Slack. This is the Go version.

Copy config.json.sample to config.json and put your settings there.

